FROM node:14.9.0-alpine3.11 as build-stage
LABEL maintainer="fikrirnurhidayat@gmail.com"
WORKDIR /app
COPY package.json yarn.lock ./
RUN set -xe \
 && yarn install
COPY . .
RUN set -xe \
 && yarn build --mode production 

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
RUN set -xe \
 && sed -i 's/listen[[:space:]]*80;/listen 8080;/g' /etc/nginx/conf.d/default.conf \
 && sed -i 's/index[[:space:]]*index\.html[[:space:]]*index\.htm;/try_files $uri $uri\/ \/index.html;/g' /etc/nginx/conf.d/default.conf
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
